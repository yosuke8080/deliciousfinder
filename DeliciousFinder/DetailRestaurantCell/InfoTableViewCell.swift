//
//  InfoTableViewCell.swift
//  DeliciousFinder
//
//  Created by YOSUKE on 3/4/18.
//  Copyright © 2018 YOSUKE. All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell {

    @IBOutlet var restaurantName: UILabel!
    @IBOutlet var phoneNum: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
