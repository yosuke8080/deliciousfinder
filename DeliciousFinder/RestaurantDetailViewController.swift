//
//  RestaurantDetailViewController.swift
//  DeliciousFinder
//
//  Created by YOSUKE on 3/4/18.
//  Copyright © 2018 YOSUKE. All rights reserved.
//

import UIKit
import MapKit
import Font_Awesome_Swift


class RestaurantDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    let myImage: UIImage = UIImage(named: "slide_kodawari_1.jpg")!
    
    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "ThumbnailTableViewCell", bundle: nil), forCellReuseIdentifier: "pic")
        self.tableView.register(UINib(nibName: "InfoTableViewCell", bundle: nil), forCellReuseIdentifier: "info")
        self.tableView.register(UINib(nibName: "DetailMapTableViewCell", bundle: nil), forCellReuseIdentifier: "map")


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var num:CGFloat = 0.0
        if indexPath.row == 0{
            num = 154.0
        }else if indexPath.row == 1{
            num =  102.0
            // detail restaurant
        }else if indexPath.row == 2{
            num = 373.0
        }
        
        return num
        
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        switch indexPath.row{
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "pic", for: indexPath) as! ThumbnailTableViewCell
                cell.thumbnail.image = myImage
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "map", for: indexPath) as! DetailMapTableViewCell
                // 中心点の緯度経度.
                let myLat: CLLocationDegrees = 37.506804
                let myLon: CLLocationDegrees = 139.930531
                let myCoordinate: CLLocationCoordinate2D = CLLocationCoordinate2DMake(myLat, myLon)
                
                // 縮尺.
                let myLatDist : CLLocationDistance = 100
                let myLonDist : CLLocationDistance = 100
                
                // Regionを作成.
                let myRegion: MKCoordinateRegion = MKCoordinateRegionMakeWithDistance(myCoordinate, myLatDist, myLonDist);
                // MapViewに反映.
                cell.mapRestaruant.setRegion(myRegion, animated: true)
                
                return cell

            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "info", for: indexPath) as! InfoTableViewCell
                cell.restaurantName.setFAIcon(icon: .FAGithub, iconSize: 25)
                cell.phoneNum.setFAIcon(icon: .FAGithub, iconSize: 25)
                return cell
            
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "pic", for: indexPath) as! ThumbnailTableViewCell
                cell.thumbnail.image = myImage
                return cell
            
            
        }
        
        


    }

}
