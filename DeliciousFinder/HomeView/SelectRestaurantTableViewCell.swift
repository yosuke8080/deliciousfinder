//
//  SelectRestaurantTableViewCell.swift
//  DeliciousFinder
//
//  Created by YOSUKE on 3/28/18.
//  Copyright © 2018 YOSUKE. All rights reserved.
//

import UIKit
import GooglePlaces

class SelectRestaurantTableViewCell: UITableViewCell {

    @IBOutlet var thumbnail: UIImageView!
    @IBOutlet var photoMeta: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        loadFirstPhotoForPlace(placeID: "ChIJwW7kiuSOAGARVCJpaROBazo")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadFirstPhotoForPlace(placeID: String) {
        GMSPlacesClient.shared().lookUpPhotos(forPlaceID: placeID) { (photos, error) -> Void in
            if let error = error {
                // TODO: handle the error.
                print("Error: \(error.localizedDescription)")
            } else {
                
                if let firstPhoto = photos?.results[3] {
                    print(photos?.results)
                    self.loadImageForMetadata(photoMetadata: firstPhoto)
                }
            }
        }
    }
    
    func loadImageForMetadata(photoMetadata: GMSPlacePhotoMetadata) {
        GMSPlacesClient.shared().loadPlacePhoto(photoMetadata, callback: {
            (photo, error) -> Void in
            if let error = error {
                // TODO: handle the error.
                print("Error: \(error.localizedDescription)")
            } else {
                self.thumbnail.image = photo;
//                self.photoMeta.text = photoMetadata.attributions
            }
        })
    }
    
    
}
