//
//  Realm.swift
//  DeliciousFinder
//
//  Created by YOSUKE on 3/28/18.
//  Copyright © 2018 YOSUKE. All rights reserved.
//

import Foundation
import RealmSwift

class Restaurant: Object {
    //Realmクラスのインスタンス
    static let realm = try! Realm()
    //id
    @objc dynamic var id = 0
    @objc dynamic var tabelog_url = ""
    @objc dynamic var name = ""
    @objc dynamic var address = ""
    @objc dynamic var pref = ""
    @objc dynamic var zip = ""
    @objc dynamic var tel = ""
    @objc dynamic var latitude: Double = 0.0
    @objc dynamic var longitude:Double = 0.0
    @objc dynamic var price = ""
    @objc dynamic var category = ""
    @objc dynamic var rate : Float = 0.0
    @objc dynamic var moyori = ""
    @objc dynamic var transportation = ""
    @objc dynamic var hours = ""
    @objc dynamic var holiday = ""
    @objc dynamic var website = ""
    @objc dynamic var favorite = false


    
    override static func primaryKey() -> String? {
        return "id"
    }
    //Create新規追加用インスタンス生成メソッド
    static func create() -> Restaurant {
        let res = Restaurant()
        res.id = lastId()
        
//        if shop.id == 0 {
//            shop.id = 2
//        }
        return res
        
    }
    //インスタンス保存用のメソッド
    func save() {
        try! Restaurant.realm.write {
            Restaurant.realm.add(self)
        }
    }
    //インスタンス削除用メソッド
    func delete() {
        try! Restaurant.realm.write {
            Restaurant.realm.delete(self)
        }
    }
    //プライマリーキー作成メソッドlastID
    static func lastId() -> Int {
        if let res = realm.objects(Restaurant.self).sorted(byKeyPath: "id").last {
            return res.id + 1
        }else {
            return 1
        }
    }
    //loadAll（順番はわからん読み込み順。並び替えしてない）
    static func loadAll() -> [Restaurant] {
        let ress = realm.objects(Restaurant.self)
        var resList: [Restaurant] = []
        for res in ress {
            resList.append(res)
        }
        return resList
    }
}


class Favorite: Object {
    //Realmクラスのインスタンス
    static let realm = try! Realm()
    //id
    @objc dynamic var id = 0
    @objc dynamic var favorite: Restaurant?
    @objc dynamic var memo:String = ""

    
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    //Create新規追加用インスタンス生成メソッド
    static func create() -> Favorite {
        let favorite = Favorite()
        favorite.id = lastId()
        return favorite
    }
    
    //インスタンス保存用のメソッド
    func save() {
        try! Favorite.realm.write {
            Favorite.realm.add(self)
        }
    }
    //インスタンス削除用メソッド
    func delete() {
        try! Favorite.realm.write {
            Favorite.realm.delete(self)
        }
    }
    //プライマリーキー作成メソッドlastID
    static func lastId() -> Int {
        if let favorite = realm.objects(Favorite.self).sorted(byKeyPath: "id").last {
            return favorite.id + 1
        }else {
            return 1
        }
    }
    //loadAll（順番はわからん読み込み順。並び替えしてない）
    static func loadAll() -> [Favorite] {
        let favorites = realm.objects(Favorite.self)
        var favoriteList: [Favorite] = []
        for favorite in favorites {
            favoriteList.append(favorite)
        }
        return favoriteList
    }
}


    
    
//
////
////  RealmDB.swift
////  kintore
////
////  Created by 中山 陽介 on 12/22/15.
////  Copyright © 2015 Yosuke Nakayama. All rights reserved.
////
//
//import Foundation
//import RealmSwift
//
//class Menu: Object {
//    //Realmクラスのインスタンス
//    static let realm = try! Realm()
//    //id
//    dynamic var id = 0
//    dynamic var name = ""
//    var trainings = List<TrainingName>()
//    override static func primaryKey() -> String? {
//        return "id"
//    }
//    //Create新規追加用インスタンス生成メソッド
//    static func create() -> Menu {
//        let menu = Menu()
//        menu.id = lastId()
//
//        if menu.id == 0 {
//            menu.id = 2
//        }
//        return menu
//
//    }
//    //インスタンス保存用のメソッド
//    func save() {
//        try! Menu.realm.write {
//            Menu.realm.add(self)
//        }
//    }
//    //インスタンス削除用メソッド
//    func delete() {
//        try! Menu.realm.write {
//            Menu.realm.delete(self)
//        }
//    }
//    //プライマリーキー作成メソッドlastID
//    static func lastId() -> Int {
//        if let menu = realm.objects(Menu).sorted("id").last {
//            return menu.id + 1
//        }else {
//            return 1
//        }
//
//    }
//    //loadAll（順番はわからん読み込み順。並び替えしてない）
//    static func loadAll() -> [Menu] {
//        let menus = realm.objects(Menu)
//        var menuList: [Menu] = []
//        for menu in menus {
//            menuList.append(menu)
//        }
//        return menuList
//    }
//}
//
//class Category: Object {
//    static let realm = try! Realm()
//    dynamic var id = 0
//    dynamic var name = ""
//    let trainings = List<TrainingName>()
//
//    override static func primaryKey() -> String? {
//        return "id"
//    }
//
//
//    //Create新規追加用インスタンス生成メソッド
//    static func create() -> Category {
//        let category = Category()
//        category.id = lastId()
//        return category
//
//
//    }
//    //インスタンス保存用のメソッド
//    func save() {
//        try! Category.realm.write {
//            Category.realm.add(self)
//        }
//    }
//    //インスタンス削除用メソッド
//    func delete() {
//        try! Category.realm.write {
//            Category.realm.delete(self)
//        }
//    }
//    //プライマリーキー作成メソッドlastID
//    static func lastId() -> Int {
//        if let user = realm.objects(Category).sorted("id").last {
//            return user.id + 1
//        }else {
//            return 1
//        }
//
//    }
//    //loadAll（順番はわからん読み込み順。並び替えしてない）
//    static func loadAll() -> [Category] {
//        let categories = realm.objects(Category)
//        var categoryList: [Category] = []
//        for category in categories {
//            categoryList.append(category)
//        }
//        return categoryList
//    }
//}
//
//
//class TrainingName: Object {
//    static let realm = try! Realm()
//    dynamic var id = 0
//    dynamic var name = ""
//    dynamic var memo = ""
//    dynamic var category :Category?
//
//    var categories: [Category] {
//        // Realm doesn't persist this property because it only has a getter defined
//        // Define "owners" as the inverse relationship to Person.dogs
//        return linkingObjects(Category.self, forProperty: "category")
//    }
//
//    var menus: [Menu] {
//        return linkingObjects(Menu.self, forProperty: "name")
//    }
//    override static func primaryKey() -> String? {
//        return "id"
//    }
//
//    //Create
//    static func create() -> TrainingName {
//        let trainingName = TrainingName()
//        trainingName.id = lastId()
//        return trainingName
//
//    }
//
//    //プライマリーキー作成メソッドlastID
//    static func lastId() -> Int {
//        if let name = realm.objects(TrainingName).sorted("id").last {
//            return name.id + 1
//        }else {
//            return 1
//        }
//    }
//    //インスタンス保存用のメソッド
//    func save() {
//        try! TrainingName.realm.write {
//            TrainingName.realm.add(self)
//        }
//    }
//    //インスタンス削除用メソッド
//    func delete() {
//        try! TrainingName.realm.write {
//            TrainingName.realm.delete(self)
//        }
//    }
//
//    //loadAll（順番はわからん読み込み順。並び替えしてない）
//    static func loadAll() -> [TrainingName] {
//        let trainings = realm.objects(TrainingName)
//        var trainingList: [TrainingName] = []
//        for training in trainings {
//            trainingList.append(training)
//        }
//        return trainingList
//    }
//}
//
//class SetTraining: Object{
//
//    static let realm = try! Realm()
//    dynamic var id = 0
//    dynamic var setNo = 0
//    dynamic var reps = 0
//    dynamic var weight = 0
//    dynamic var weightKg = true
//    //TrainingNameいれる。
//    dynamic var training:TrainingName!
//
//
//
//
//    //primary
//    override static func primaryKey() -> String? {
//        return "id"
//    }
//
//    //Create
//    static func create() -> SetTraining {
//        let setTraining = SetTraining()
//        setTraining.id = lastId()
//        return setTraining
//
//    }
//
//
//    //プライマリーキー作成メソッドlastID
//    static func lastId() -> Int {
//        if let name = realm.objects(SetTraining).sorted("id").last {
//            return name.id + 1
//        }else {
//            return 1
//        }
//    }
//
//    //インスタンス保存用のメソッド
//    func save() {
//        try! SetTraining.realm.write {
//            SetTraining.realm.add(self)
//        }
//    }
//    //インスタンス削除用メソッド
//    func delete() {
//        try! SetTraining.realm.write {
//            SetTraining.realm.delete(self)
//        }
//    }
//
//    //loadAll（順番はわからん読み込み順。並び替えしてない）
//    static func loadAll() -> [SetTraining] {
//        let trainingsSets = realm.objects(SetTraining)
//        var trainingSetList: [SetTraining] = []
//        for trainingSet in trainingsSets {
//            trainingSetList.append(trainingSet)
//        }
//        return trainingSetList
//    }
//}
//
//
//
//class LogTraining: Object{
//
//    static let realm = try! Realm()
//    dynamic var id = 0
//    dynamic var setNo = 0
//    dynamic var reps = 0
//    dynamic var weight = 0
//    dynamic var weightKg = true
//    dynamic var date :NSDate!
//    //TrainingNameいれる。
//    dynamic var training:TrainingName!
//
//
//
//
//    //primary
//    override static func primaryKey() -> String? {
//        return "id"
//    }
//
//    //Create
//    static func create() -> LogTraining {
//        let logTraining = LogTraining()
//        logTraining.id = lastId()
//        return logTraining
//    }
//
//
//    //プライマリーキー作成メソッドlastID
//    static func lastId() -> Int {
//        if let name = realm.objects(LogTraining).sorted("id").last {
//            return name.id + 1
//        }else {
//            return 1
//        }
//    }
//
//    //インスタンス保存用のメソッド
//    func save() {
//        try! LogTraining.realm.write {
//            LogTraining.realm.add(self)
//        }
//    }
//    //インスタンス削除用メソッド
//    func delete() {
//        try! LogTraining.realm.write {
//            LogTraining.realm.delete(self)
//        }
//    }
//
//
//    //ログが一番データ量が多くなるだろうから、考慮必要性あり
//    //loadAll（順番はわからん読み込み順。並び替えしてない）
//    static func loadAll() -> [LogTraining] {
//        let logTrainingsSets = realm.objects(LogTraining).sorted("date")
//        var logTrainingSetList: [LogTraining] = []
//        for logTrainingSet in logTrainingsSets {
//            logTrainingSetList.append(logTrainingSet)
//        }
//        return logTrainingSetList
//    }
//
//
//    //時間を
//    static func changeTimeToUS(date: NSDate, timeZone: String) -> String{
//        let df = NSDateFormatter()
//        df.timeStyle = .ShortStyle
//        df.dateStyle = .ShortStyle
//        df.locale = NSLocale(localeIdentifier: timeZone )
//        return df.stringFromDate(date)
//
//    }
//}
//
//class Personal: Object {
//    //Realmクラスのインスタンス
//    static let realm = try! Realm()
//    //id
//    dynamic var id = 0
//    dynamic var weight = 0
//    dynamic var percentage = 0.0
//    dynamic var todayMemo = ""
//    dynamic var date :NSDate!
//
//    override static func primaryKey() -> String? {
//        return "id"
//    }
//    //Create新規追加用インスタンス生成メソッド
//    static func create() -> Personal {
//        let personal = Personal()
//        personal.id = lastId()
//
//        if personal.id == 0 {
//            personal.id = 2
//        }
//        return personal
//
//    }
//    //インスタンス保存用のメソッド
//    func save() {
//        try! Personal.realm.write {
//            Personal.realm.add(self)
//        }
//    }
//    //インスタンス削除用メソッド
//    func delete() {
//        try! Personal.realm.write {
//            Personal.realm.delete(self)
//        }
//    }
//    //プライマリーキー作成メソッドlastID
//    static func lastId() -> Int {
//        if let personal = realm.objects(Personal).sorted("id").last {
//            return personal.id + 1
//        }else {
//            return 1
//        }
//
//    }
//    //loadAll（順番はわからん読み込み順。並び替えしてない）
//    static func loadAll() -> [Personal] {
//        let personals = realm.objects(Personal)
//        var personalList: [Personal] = []
//        for personal in personals {
//            personalList.append(personal)
//        }
//        return personalList
//    }
//
//
//
//}
//



