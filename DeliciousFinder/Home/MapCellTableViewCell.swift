//
//  MapCellTableViewCell.swift
//  DeliciousFinder
//
//  Created by YOSUKE on 3/25/18.
//  Copyright © 2018 YOSUKE. All rights reserved.
//

import UIKit
import MapKit


class MapCellTableViewCell: UITableViewCell {

    @IBOutlet var mapView: MKMapView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
