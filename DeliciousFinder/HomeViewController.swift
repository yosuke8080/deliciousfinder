//
//  HomeViewController.swift
//  DeliciousFinder
//
//  Created by YOSUKE on 3/27/18.
//  Copyright © 2018 YOSUKE. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import GooglePlaces
import CoreLocation

struct Tabelog: Codable {
    let tabelog_url: String
    let name: String
    let address: String
    let pref: String
    let zip: String
    let tel: String
    let latitude: Float
    let longitude:Float
    let price: String
    let category: String
    let rate: String
    let moyori: String
    let transportation: String
    let hours: String
    let holiday: String
    let website: String
}

class Spot {
    let restaurantInfo: Restaurant
    let location: CLLocation
    var appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    private(set) var distance: CLLocationDistance?
    
    var targetLocation: CLLocation? {
        didSet {
            guard let location = targetLocation else {
                distance = nil
                return
            }
            if location.isEqual(location: oldValue) {
                return
            }
            distance = self.location.distance(from: location)
        }
    }
    
    init (_ lat: CLLocationDegrees, _ lng: CLLocationDegrees, _ restaurant: Restaurant) {
        self.location = CLLocation(latitude: lat, longitude: lng)
        self.restaurantInfo = restaurant
    }
    
    static var allRes: [Spot] {
        var allRes:[Spot] = []
        let queryObject = Restaurant.loadAll()
        for object in queryObject{
            let spot = Spot(object.latitude ,object.longitude ,object)
            allRes.append(spot)
        }
        return allRes
    }
    static func sortedList(nearFrom location: CLLocation) -> [Spot] {
        return self.allRes.sorted(by: { spot1, spot2 in
            spot1.targetLocation = location
            spot2.targetLocation = location
            return spot1.distance! < spot2.distance!
        })
    }

}

extension CLLocation {
    // 同じ座標かどうかを返す
    func isEqual(location: CLLocation?) -> Bool {
        if let location = location {
            return self.coordinate.latitude  == location.coordinate.latitude
                && self.coordinate.longitude == location.coordinate.longitude
        }
        return false
    }
}



class HomeViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{
    @IBOutlet var tableView: UITableView!
    var shopList: Tabelog?
    var placesClient: GMSPlacesClient!
    private let manager = CLLocationManager()
    private var monitoredRegion: CLCircularRegion?
    let realm = try! Realm()

    var appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    private(set) var distance: CLLocationDistance?
    
    var targetLocation: CLLocation? {
        didSet {
            guard let location = targetLocation else {
                distance = nil
                return
            }
            if location.isEqual(location: oldValue) {
                return
            }
            distance = self.appDelegate.myLocation.distance(from: location)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "HomeMapTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeMap")
        self.tableView.register(UINib(nibName: "SelectRestaurantTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectRestaurant")
        placesClient = GMSPlacesClient.shared()
        googlePlace()
        authForNotification()
        getJSON()
        requestPrivasyAccess()
        startMonitoring()
        let currentLocation = CLLocation(latitude: self.appDelegate.myLocation.coordinate.latitude, longitude: self.appDelegate.myLocation.coordinate.longitude)
        
        Spot.sortedList(nearFrom: currentLocation).forEach({ spot in
            // 1000mを超える場合はキロメートで表示
            let distance = spot.distance!
            let kyori = distance / 1000.0 > 1.0 ?
                "\(floor(distance / 1000.0)) キロメートル"
                :"\(floor(distance)) メートル"
//            print(kyori)
            
            print("\(spot.restaurantInfo.name) (\(kyori))")
        })
    }

    
    func authForNotification(){
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func googlePlace(){
        placesClient.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let placeLikelihoodList = placeLikelihoodList {
                let place = placeLikelihoodList.likelihoods.first?.place
                if let place = place {
                    print(place.name)
                    print(place.formattedAddress?.components(separatedBy: ", ")
                        .joined(separator: "\n"))
                }
            }
        })
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 240.0
        default:
            return 320.0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return 10
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "") {
            //            let vc2: SecondViewController = (segue.destination as? SecondViewController)!
            // ViewControllerのtextVC2にメッセージを設定
            //            vc2.textVC2 = "to VC2"
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        let next = storyboard!.instantiateViewController(withIdentifier: "detailRestaurant")
        //        self.present(next,animated: false, completion: nil)
        performSegue(withIdentifier: "detailRestaurant", sender: nil)
        //        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()!
        //        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeMap", for: indexPath) as! HomeMapTableViewCell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectRestaurant", for: indexPath) as! SelectRestaurantTableViewCell
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectRestaurant", for: indexPath) as! SelectRestaurantTableViewCell
            return cell
        }

    }
    
    @objc func showTurnOnLocationServiceAlert(_ notification: NSNotification){
        let alert = UIAlertController(title: "Turn on Location Service", message: "To use location tracking feature of the app, please turn on the location service from the Settings app.", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        
        
        present(alert, animated: true, completion: nil)
        
    }
    
    
    func getJSON(){
        guard let path = Bundle.main.path(forResource: "tabelog", ofType: "json") else { return }
        let url = URL(fileURLWithPath: path)
        do {
        let data = try Data(contentsOf: url)
        print("here")
    
        let tabelog = try JSONDecoder().decode([Tabelog].self, from: data)
        //            let json = try JSON(data: data)
        for r in tabelog{
        let shop = Restaurant.create()
        shop.tabelog_url = r.tabelog_url
        shop.name = r.name
        shop.pref = r.pref
        shop.zip = r.zip
        shop.tel = r.tel
        shop.address = r.address
        shop.latitude = Double(r.latitude)
        shop.longitude = Double(r.longitude)
        shop.price = r.price
        shop.category = r.category
        shop.rate = Float(r.rate)!
        shop.moyori = r.moyori
        shop.hours = r.hours
        shop.transportation = r.transportation
        shop.holiday = r.holiday
        shop.website = r.website
        shop.save()
    
        }
    
        } catch {
        print(error)
        }
    
    }
    
    private func requestPrivasyAccess() {
        self.manager.requestAlwaysAuthorization()
    }
    
    private func startMonitoring() {
        let status = CLLocationManager.authorizationStatus()
        if status == .denied || status == .restricted {
            return
        }
        self.manager.delegate = self
        let center = CLLocationCoordinate2DMake(37.38723661, 37.38723661)

        self.monitoredRegion = CLCircularRegion(center: center, radius: 1, identifier: "region1")
        self.manager.startMonitoring(for: self.monitoredRegion!)
    }
    
    private func stopMonitoring() {
        if let monitoredRegion = self.monitoredRegion {
            self.manager.stopMonitoring(for: monitoredRegion)
        }
      }
}

    




extension HomeViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        print("観測を開始しました！")
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("観測の開始に失敗しました！")
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("領域に入りました！")
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("領域から出ました！")
    }
}

//
//// 羽田空港のCLLocation
//let hanedaLocation = CLLocation(latitude: 35.549393, longitude: 139.779839)
//
//// 香港ビクトリアピークのCLLocation
//let peakLocation = CLLocation(latitude: 22.2760448, longitude: 114.1455266)
//
//// 羽田空港から香港ビクトリアピークまでの距離
//let distance = hanedaLocation.distance(from: peakLocation)
//
//// 1000mを超える場合はキロメートで表示
//let kyori = distance / 1000.0 > 1.0 ?
//    "\(floor(distance / 1000.0)) キロメートル"
//    :
//"\(floor(distance)) メートル"
//print(kyori)
//
